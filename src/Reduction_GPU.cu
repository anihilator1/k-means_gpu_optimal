#include <iostream>
#include <stdlib.h>
#include<chrono>
#include<unistd.h>
#include<algorithm>
#include <math.h>
#include<thrust/device_vector.h>
#include<thrust/copy.h>
#include<thrust/equal.h>
#include<thrust/execution_policy.h>
static void CheckCudaErrorAux(const char *, unsigned, const char *,
		cudaError_t);
#define CUDA_CHECK_RETURN(value) CheckCudaErrorAux(__FILE__,__LINE__, #value, value)

static void CheckCudaErrorAux(const char *file, unsigned line,
		const char *statement, cudaError_t err) {
	if (err == cudaSuccess)
		return;
	std::cerr << statement << " returned " << cudaGetErrorString(err) << "("
			<< err << ") at " << file << ":" << line << std::endl;
	exit(1);
}

using namespace std;

float* particlesXTab;
float* particlesYTab;
float* particlesZTab;

float* centroidsXTab;
float* centroidsYTab;
float* centroidsZTab;

float* resultX;
float* resultY;
float* resultZ;
float* newResultX;
float* newResultY;
float* newResultZ;

float* countParticles;

int* groupingTab;
int* newGroupingTab;

unsigned numberOfParticles =1000;
unsigned numberOfCentroids = 2;

int blockSize =32;
int blockCount;
int reductionBlocksCount;
int reductionBlocksCount2;

struct Particles {
	float* xTab;
	float* yTab;
	float* zTab;
	int size;
	Particles(int size) {
		xTab = new float[size];
		yTab = new float[size];
		zTab = new float[size];
		this->size = size;
	}
	Particles(const Particles& particles) {
		xTab = new float[particles.size];
		yTab = new float[particles.size];
		zTab = new float[particles.size];
		this->size = particles.size;
		for (int i = 0; i < particles.size; i++) {
			xTab[i] = particles.xTab[i];
			yTab[i] = particles.yTab[i];
			zTab[i] = particles.zTab[i];
		}
	}
	Particles& operator=(const Particles& particles) {
		delete[] xTab;
		delete[] yTab;
		delete[] zTab;
		xTab = new float[particles.size];
		yTab = new float[particles.size];
		zTab = new float[particles.size];
		this->size = particles.size;
		for (int i = 0; i < particles.size; i++) {
			xTab[i] = particles.xTab[i];
			yTab[i] = particles.yTab[i];
			zTab[i] = particles.zTab[i];
		}
		return *this;
	}

	~Particles() {
		delete[] xTab;
		delete[] yTab;
		delete[] zTab;
	}

};
float FloatRand() {
	return rand() / (float) RAND_MAX;
}

void DisplayParticles(Particles& particles) {
	for (int i = 0; i < particles.size; i++) {
		cout << "x:" << particles.xTab[i] << " y:" << particles.yTab[i] << " z:"
				<< particles.zTab[i] << endl;
	}
}
Particles RandParticles(int size) {
	srand(123);
	Particles particles(size);
	for (int i = 0; i < size; i++) {
		particles.xTab[i] =FloatRand();
		particles.yTab[i] =FloatRand();
		particles.zTab[i] =FloatRand();
	}
	return particles;
}



Particles InitCentroids(Particles& particles, int numberOfCentroids) {
	Particles centroids(numberOfCentroids);
	for (int i = 0; i < numberOfCentroids; i++) {
		centroids.xTab[i] = particles.xTab[i];
		centroids.yTab[i] = particles.yTab[i];
		centroids.zTab[i] = particles.zTab[i];
	}
	return centroids;
}

__device__ void centroidsToSharedMemory(float* centroidsXTab,
		float* centroidsYTab, float* centroidsZTab, float* sharedCentroidsXTab,
		float* sharedCentroidsYTab, float* sharedCentroidsZTab, unsigned idb,
		unsigned blockDim, unsigned numberOfCentroids) {
	int index = idb;
	while (index < numberOfCentroids) {
		sharedCentroidsXTab[index] = centroidsXTab[index];
		sharedCentroidsYTab[index] = centroidsYTab[index];
		sharedCentroidsZTab[index] = centroidsZTab[index];
		index += blockDim;
	}
}

__device__ float calculateDistance(float x1, float y1, float z1, float x2,
		float y2, float z2) {
	float result = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)
			+ (z1 - z2) * (z1 - z2);
	return result;
}

__global__ void GroupParticlesByCentroidsKernel(float* particlesXTab,
		float* particlesYTab, float* particlesZTab, float* centroidsXTab,
		float* centroidsYTab, float* centroidsZTab, int* groupingTab,
		unsigned numberOfParicles, unsigned numberOfCentroids) {
	extern __shared__ float sharedMemory[];
	float* sharedCentroidsXTab = sharedMemory;
	float* sharedCentroidsYTab = sharedMemory + numberOfCentroids;
	float* sharedCentroidsZTab = sharedCentroidsYTab + numberOfCentroids;

	unsigned idx = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned idb = threadIdx.x;
	centroidsToSharedMemory(centroidsXTab, centroidsYTab, centroidsZTab,
			sharedCentroidsXTab, sharedCentroidsYTab, sharedCentroidsZTab, idb,
			blockDim.x, numberOfCentroids);
	if (idx < numberOfParicles) {
		float minDistance = calculateDistance(particlesXTab[idx],
				particlesYTab[idx], particlesZTab[idx], sharedCentroidsXTab[0],
				sharedCentroidsYTab[0], sharedCentroidsZTab[0]);
		int minIndex = 0;
		for (int i = 1; i < numberOfCentroids; i++) {
			float distance = calculateDistance(particlesXTab[idx],
					particlesYTab[idx], particlesZTab[idx],
					sharedCentroidsXTab[i], sharedCentroidsYTab[i],
					sharedCentroidsZTab[i]);
			if (minDistance > distance) {
				minDistance = distance;
				minIndex = i;
			}
		}
		groupingTab[idx] = minIndex;
	}
}

template<unsigned int blockSize>
__device__ void WarpReduce(volatile float *sdata, unsigned int tid) {
	if (blockSize >= 64)
		sdata[tid] += sdata[tid + 32];
	if (blockSize >= 32)
		sdata[tid] += sdata[tid + 16];
	if (blockSize >= 16)
		sdata[tid] += sdata[tid + 8];
	if (blockSize >= 8)
		sdata[tid] += sdata[tid + 4];
	if (blockSize >= 4)
		sdata[tid] += sdata[tid + 2];
	if (blockSize >= 2)
		sdata[tid] += sdata[tid + 1];
}

template <unsigned int blockSize>
__device__ void Reduce(float *g_idata,int* groupingTable, float *g_odata,volatile float *sdata, unsigned int n,int centroid,unsigned count)
{
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	while (i < n)
	{
		sdata[tid] +=(groupingTable[i]==centroid)?g_idata[i]:0;
		sdata[tid] += (i+blockSize<n)?(groupingTable[i+blockSize]==centroid?g_idata[i+blockSize]:0):0;
		i += gridSize;
	}
	__syncthreads();
	if (blockSize >= 512)
	{
		if (tid < 256)
		{
			sdata[tid] += sdata[tid + 256];
		}
		__syncthreads();
	}
	if(blockSize >= 256)
	{
		if (tid < 128)
		{
			sdata[tid] += sdata[tid + 128];
		}
		__syncthreads();
	}

	if (blockSize >= 128)
	{
		if (tid <64)
		{
			sdata[tid] += sdata[tid +64];
		}
		__syncthreads();
	}
	if (tid < 32) WarpReduce<blockSize>(sdata,tid);
	if (tid == 0) g_odata[blockIdx.x] = sdata[0]/count;
}

template <unsigned int blockSize>
__device__  int Count(int* groupingTable, float *g_odata,volatile float *sdata, unsigned int n,int centroid)
{
	unsigned int tid = threadIdx.x;
		unsigned int i = blockIdx.x*(blockSize*2) + tid;
		unsigned int gridSize = blockSize*2*gridDim.x;
		sdata[tid] = 0;

		while (i < n)
		{
			sdata[tid] +=(groupingTable[i]==centroid)?1:0;
			sdata[tid] += (i+blockSize<n)?(groupingTable[i+blockSize]==centroid?1:0):0;
			i += gridSize;
		}
		__syncthreads();
		if (blockSize >= 512)
		{
			if (tid < 256)
			{
				sdata[tid] += sdata[tid + 256];
			}
			__syncthreads();
		}
		if(blockSize >= 256)
		{
			if (tid < 128)
			{
				sdata[tid] += sdata[tid + 128];
			}
			__syncthreads();
		}

		if (blockSize >= 128)
		{
			if (tid <64)
			{
				sdata[tid] += sdata[tid +64];
			}
			__syncthreads();
		}
		if (tid < 32) WarpReduce<blockSize>(sdata,tid);
		if (tid == 0)
		{
			g_odata[blockIdx.x] = sdata[0];
			return (int)sdata[0];
		}
}


template <unsigned int blockSize>
__global__ void ReduceByBlocks(float* particlesXTab,float* particlesYTab, float* particlesZTab,int *groupingTab,
		unsigned numberOfParicles,float *resultX,float *resultY,float *resultZ ,int centroid,float* countParticles)
{
	extern __shared__ float sdata[];
	int count=Count<blockSize>(groupingTab,countParticles,sdata,numberOfParicles,centroid);
	__syncthreads();
	Reduce<blockSize>(particlesXTab,groupingTab,resultX,sdata,numberOfParicles,centroid,count);
	__syncthreads();
	Reduce<blockSize>(particlesYTab,groupingTab,resultY,sdata,numberOfParicles,centroid,count);
	__syncthreads();
	Reduce<blockSize>(particlesZTab,groupingTab,resultZ,sdata,numberOfParicles,centroid,count);
	__syncthreads();

}


template <unsigned int blockSize>
__device__ void LoopReduce(float *g_idata, float *g_odata,volatile float *sdata, unsigned int n,unsigned count)
{
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	while (i < n)
	{
		sdata[tid] +=isnan(g_idata[i])?0:g_idata[i];
		sdata[tid] += (i+blockSize<n)?(isnan(g_idata[i])?0:g_idata[i]):0;
		i += gridSize;
	}
	__syncthreads();
	if (blockSize >= 512)
	{
		if (tid < 256)
		{
			sdata[tid] += sdata[tid + 256];
		}
		__syncthreads();
	}
	if(blockSize >= 256)
	{
		if (tid < 128)
		{
			sdata[tid] += sdata[tid + 128];
		}
		__syncthreads();
	}

	if (blockSize >= 128)
	{
		if (tid <64)
		{
			sdata[tid] += sdata[tid +64];
		}
		__syncthreads();
	}
	if (tid < 32) WarpReduce<blockSize>(sdata,tid);
	if (tid == 0)
	{
		g_odata[blockIdx.x] = sdata[0]/count;
	}

}

template <unsigned int blockSize>
__device__ unsigned LoopCount(float *g_idata,volatile float *sdata, unsigned int n)
{
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	while (i < n)
	{
		sdata[tid] +=isnan(g_idata[i])?0:1;
		sdata[tid] += (i+blockSize<n)?(isnan(g_idata[i])?0:1):0;
		i += gridSize;
	}
	__syncthreads();
	if (blockSize >= 512)
	{
		if (tid < 256)
		{
			sdata[tid] += sdata[tid + 256];
		}
		__syncthreads();
	}
	if(blockSize >= 256)
	{
		if (tid < 128)
		{
			sdata[tid] += sdata[tid + 128];
		}
		__syncthreads();
	}

	if (blockSize >= 128)
	{
		if (tid <64)
		{
			sdata[tid] += sdata[tid +64];
		}
		__syncthreads();
	}
	if (tid < 32) WarpReduce<blockSize>(sdata,tid);
	if (tid == 0)
	{
		return sdata[0];
	}

}

template <unsigned int blockSize>
__global__ void LoopReduction(float* particlesXTab,float* particlesYTab, float* particlesZTab,
		unsigned numberOfParicles,float *resultX,float *resultY,float *resultZ)
{
	extern __shared__ float sdata[];
	unsigned count=LoopCount<blockSize>(particlesXTab,sdata,numberOfParicles);
	LoopReduce<blockSize>(particlesXTab,resultX,sdata,numberOfParicles,count);
	__syncthreads();
	LoopReduce<blockSize>(particlesYTab,resultY,sdata,numberOfParicles,count);
	__syncthreads();
	LoopReduce<blockSize>(particlesZTab,resultZ,sdata,numberOfParicles,count);
	__syncthreads();

}


template <unsigned int blockSize>
__device__ void FinalReduce(float *g_idata, float *g_odata,volatile float *sdata, unsigned int n,unsigned centroid)
{
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*(blockSize*2) + tid;
	unsigned int gridSize = blockSize*2*gridDim.x;
	sdata[tid] = 0;

	while (i < n)
	{
		sdata[tid] +=isnan(g_idata[i])?0:g_idata[i];
		sdata[tid] += (i+blockSize<n)?(isnan(g_idata[i])?0:g_idata[i]):0;
		i += gridSize;
	}
	__syncthreads();
	if (blockSize >= 512)
	{
		if (tid < 256)
		{
			sdata[tid] += sdata[tid + 256];
		}
		__syncthreads();
	}
	if(blockSize >= 256)
	{
		if (tid < 128)
		{
			sdata[tid] += sdata[tid + 128];
		}
		__syncthreads();
	}

	if (blockSize >= 128)
	{
		if (tid <64)
		{
			sdata[tid] += sdata[tid +64];
		}
		__syncthreads();
	}
	if (tid < 32) WarpReduce<blockSize>(sdata,tid);
	if (tid == 0)
	{
		g_odata[centroid] = sdata[0]/n;
	}

}
template <unsigned int blockSize>
__global__ void FinalReduction(float* particlesXTab,float* particlesYTab, float* particlesZTab,
		unsigned numberOfParicles,float *resultX,float *resultY,float *resultZ,unsigned centroid)
{
	extern __shared__ float sdata[];
	FinalReduce<blockSize>(particlesXTab,resultX,sdata,numberOfParicles,centroid);
	__syncthreads();
	FinalReduce<blockSize>(particlesYTab,resultY,sdata,numberOfParicles,centroid);
	__syncthreads();
	FinalReduce<blockSize>(particlesZTab,resultZ,sdata,numberOfParicles,centroid);
	__syncthreads();

}


void InitKernel(Particles particles, Particles centroids) {
	int particlesDataSize = particles.size;
	int centroidsDataSize = centroids.size;
	blockCount = (numberOfParticles + blockSize - 1) / blockSize;

	unsigned tmp= (numberOfParticles/log10(numberOfParticles) + blockSize - 1) / blockSize;
	reductionBlocksCount=(tmp+1)/2;

	unsigned tmp2=(reductionBlocksCount/log10(reductionBlocksCount)+blockSize-1)/blockSize;
	reductionBlocksCount2=(tmp2+1)/2;

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&particlesXTab,sizeof(float) * particlesDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&particlesYTab,sizeof(float) * particlesDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&particlesZTab,sizeof(float) * particlesDataSize));

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&centroidsXTab,sizeof(float) * centroidsDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&centroidsYTab,sizeof(float) * centroidsDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&centroidsZTab,sizeof(float) * centroidsDataSize));

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&groupingTab,sizeof(int) * particlesDataSize));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&newGroupingTab,sizeof(int) * particlesDataSize));

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&resultX,sizeof(float) * reductionBlocksCount));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&resultY,sizeof(float) * reductionBlocksCount));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&resultZ,sizeof(float) * reductionBlocksCount));

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&newResultX,sizeof(float) * reductionBlocksCount2));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&newResultY,sizeof(float) * reductionBlocksCount2));
	CUDA_CHECK_RETURN(cudaMalloc((void ** )&newResultZ,sizeof(float) * reductionBlocksCount2));

	CUDA_CHECK_RETURN(cudaMalloc((void ** )&countParticles,sizeof(float) * reductionBlocksCount));


	CUDA_CHECK_RETURN(cudaMemcpy(particlesXTab, particles.xTab,sizeof(float) * particlesDataSize, cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(particlesYTab, particles.yTab,sizeof(float) * particlesDataSize, cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(particlesZTab, particles.zTab,sizeof(float) * particlesDataSize, cudaMemcpyHostToDevice));

	CUDA_CHECK_RETURN(cudaMemcpy(centroidsXTab, centroids.xTab,sizeof(float) * centroidsDataSize, cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(centroidsYTab, centroids.yTab,sizeof(float) * centroidsDataSize, cudaMemcpyHostToDevice));
	CUDA_CHECK_RETURN(cudaMemcpy(centroidsZTab, centroids.zTab,sizeof(float) * centroidsDataSize, cudaMemcpyHostToDevice));

}
void FreeDeviceMemory() {
	CUDA_CHECK_RETURN(cudaFree(particlesXTab));
	CUDA_CHECK_RETURN(cudaFree(particlesYTab));
	CUDA_CHECK_RETURN(cudaFree(particlesZTab));

	CUDA_CHECK_RETURN(cudaFree(centroidsXTab));
	CUDA_CHECK_RETURN(cudaFree(centroidsYTab));
	CUDA_CHECK_RETURN(cudaFree(centroidsZTab));

	CUDA_CHECK_RETURN(cudaFree(resultX));
	CUDA_CHECK_RETURN(cudaFree(resultY));
	CUDA_CHECK_RETURN(cudaFree(resultZ));

	CUDA_CHECK_RETURN(cudaFree(countParticles));

	CUDA_CHECK_RETURN(cudaFree(groupingTab));
	CUDA_CHECK_RETURN(cudaFree(newGroupingTab));

}
bool flag=true;
void DisplayResult(float* deviceResult,int size)
{
	float* result = new float[size];
	CUDA_CHECK_RETURN(cudaMemcpy(result, deviceResult, sizeof(float) * size,cudaMemcpyDeviceToHost));
	cout<<"result:"<<endl;
	for(int i=0;i<size;i++)
	{
		cout<<result[i]<<" ";
	}
	cout<<endl;
}
void DisplayResult(int* deviceResult,int size)
{
	int* result = new int[size];
	CUDA_CHECK_RETURN(cudaMemcpy(result, deviceResult, sizeof(int) * size,cudaMemcpyDeviceToHost));
	cout<<"result:"<<endl;
	for(int i=0;i<size;i++)
	{
		cout<<result[i]<<" ";
	}
	cout<<endl;
}

int* GroupParticlesByCentroids() {

	thrust::copy(thrust::device,groupingTab,groupingTab+numberOfParticles,newGroupingTab);
	GroupParticlesByCentroidsKernel<<<blockCount, blockSize,
			numberOfCentroids * 3 * sizeof(float)>>>(particlesXTab,
			particlesYTab, particlesZTab, centroidsXTab, centroidsYTab,
			centroidsZTab, groupingTab, numberOfParticles, numberOfCentroids);

}
void FirstReductionInit(unsigned centroid)
{
	if(reductionBlocksCount==1)
	{
		switch(blockSize)
			{
				case 2048:
					ReduceByBlocks<2048><<<reductionBlocksCount,blockSize,2048*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 1024:
					ReduceByBlocks<1024><<<reductionBlocksCount,blockSize,1024*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 512:
					ReduceByBlocks<512><<<reductionBlocksCount,blockSize,512*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 256:
					ReduceByBlocks<256><<<reductionBlocksCount,blockSize,256*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 128:
					ReduceByBlocks<128><<<reductionBlocksCount,blockSize,128*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 64:
					ReduceByBlocks<64><<<reductionBlocksCount,blockSize,64*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 32:
					ReduceByBlocks<32><<<reductionBlocksCount,blockSize,32*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 16:
					ReduceByBlocks<16><<<reductionBlocksCount,blockSize,16*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 8:
					ReduceByBlocks<8><<<reductionBlocksCount,blockSize,8*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 4:
					ReduceByBlocks<4><<<reductionBlocksCount,blockSize,4*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 2:
					ReduceByBlocks<2><<<reductionBlocksCount,blockSize,2*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
				case 1:
					ReduceByBlocks<1><<<reductionBlocksCount,blockSize,1*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,centroidsXTab+centroid,centroidsYTab+centroid,centroidsZTab+centroid,centroid,countParticles);
					break;
			}
	}
	switch(blockSize)
	{
		case 2048:
			ReduceByBlocks<2048><<<reductionBlocksCount,blockSize,2048*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 1024:
			ReduceByBlocks<1024><<<reductionBlocksCount,blockSize,1024*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 512:
			ReduceByBlocks<512><<<reductionBlocksCount,blockSize,512*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 256:
			ReduceByBlocks<256><<<reductionBlocksCount,blockSize,256*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 128:
			ReduceByBlocks<128><<<reductionBlocksCount,blockSize,128*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 64:
			ReduceByBlocks<64><<<reductionBlocksCount,blockSize,64*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 32:
			ReduceByBlocks<32><<<reductionBlocksCount,blockSize,32*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 16:
			ReduceByBlocks<16><<<reductionBlocksCount,blockSize,16*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 8:
			ReduceByBlocks<8><<<reductionBlocksCount,blockSize,8*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 4:
			ReduceByBlocks<4><<<reductionBlocksCount,blockSize,4*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 2:
			ReduceByBlocks<2><<<reductionBlocksCount,blockSize,2*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
		case 1:
			ReduceByBlocks<1><<<reductionBlocksCount,blockSize,1*sizeof(float)>>>(particlesXTab,particlesYTab,particlesZTab,groupingTab,numberOfParticles,resultX,resultY,resultZ,centroid,countParticles);
			break;
	}
}




void LoopReductionInit(float* resultXL,float* resultYL,float* resultZL,float* newResultXL,float* newResultYL, float* newResultZL,unsigned size)
{
	unsigned tmp= (size/log10(size) + blockSize - 1) / blockSize;
	unsigned blocks=(tmp+1)/2;
	switch(blockSize)
		{
			case 2048:
				LoopReduction<2048><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 1024:
				LoopReduction<1024><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 512:
				LoopReduction<512><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 256:
				LoopReduction<256><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 128:
				LoopReduction<128><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 64:
				LoopReduction<64><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 32:
				LoopReduction<32><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 16:
				LoopReduction<16><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 8:
				LoopReduction<8><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 4:
				LoopReduction<4><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 2:
				LoopReduction<2><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
			case 1:
				LoopReduction<1><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL);
				break;
		}
}
void FinalReductionInit(float* resultXL,float* resultYL,float* resultZL,float* newResultXL,float* newResultYL, float* newResultZL,unsigned size,unsigned centroid)
{
	unsigned tmp= (size/log10(size) + blockSize - 1) / blockSize;
	unsigned blocks=(tmp+1)/2;
	switch(blockSize)
		{
			case 2048:
				FinalReduction<2048><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 1024:
				FinalReduction<1024><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 512:
				FinalReduction<512><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 256:
				FinalReduction<256><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 128:
				FinalReduction<128><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 64:
				FinalReduction<64><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 32:
				FinalReduction<32><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 16:
				FinalReduction<16><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 8:
				FinalReduction<8><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 4:
				FinalReduction<4><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 2:
				FinalReduction<2><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
			case 1:
				FinalReduction<1><<<blocks,blockSize,2048*sizeof(float)>>>(resultXL,resultYL,resultZL,size,newResultXL,newResultYL,newResultZL,centroid);
				break;
		}
}
void FirstReduction(unsigned centroid)
{
	FirstReductionInit(centroid);
}
bool  LoopReduction(int centroid)
{
	unsigned size=reductionBlocksCount;
	bool flag=true;
	//cout<<"reduction looop"<<endl;
	while(size!=1)
	{
		unsigned tmp= (size/log10(size) + blockSize - 1) / blockSize;
		unsigned preSize=(tmp+1)/2;
		if(flag)
		{
			if(preSize==1)
			{
				FinalReductionInit(resultX,resultY,resultZ,centroidsXTab,centroidsYTab,centroidsZTab,size,centroid);
				//cout<<"final"<<endl;
				//DisplayResult(centroidsXTab+centroid,1);
			}
			else
			{
				LoopReductionInit(resultX,resultY,resultZ,newResultX,newResultY,newResultZ,size);
				//DisplayResult(newResultX,preSize);
			}

			flag=!flag;
		}
		else
		{
			if(preSize==1)
			{
				FinalReductionInit(newResultX,newResultY,newResultZ,centroidsXTab,centroidsYTab,centroidsZTab,size,centroid);
				//cout<<"final"<<endl;
				//DisplayResult(centroidsXTab+centroid,1);
			}
			else
			{
				LoopReductionInit(newResultX,newResultY,newResultZ,resultX,resultY,resultZ,size);
				//DisplayResult(resultX,preSize);

			}
			flag=!flag;
		}
		size=preSize;
	}
	return flag;
}

void ComputeNewCentroidsPosition()
{
	for(int i=0;i<numberOfCentroids;i++)
	{
		FirstReduction(i);
		//cout<<"after first redcution"<<endl;
		//DisplayResult(resultX,reductionBlocksCount);
		//cout<<endl;
	    LoopReduction(i);
	}


}
bool Condition()
{
	return thrust::equal(thrust::device,groupingTab,groupingTab+numberOfParticles,newGroupingTab);
}
void ComputeCentroids(Particles& particles) {
	GroupParticlesByCentroids();
	while(true)
	{
	  ComputeNewCentroidsPosition();
	  GroupParticlesByCentroids();
	  if(Condition())
	  {
		  return ;
	  }
	}

}
Particles getCentroids()
{
	Particles centroids(numberOfCentroids);
	CUDA_CHECK_RETURN(cudaMemcpy(centroids.xTab, centroidsXTab, sizeof(float) * numberOfCentroids,cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaMemcpy(centroids.yTab, centroidsYTab, sizeof(float) * numberOfCentroids,cudaMemcpyDeviceToHost));
	CUDA_CHECK_RETURN(cudaMemcpy(centroids.zTab, centroidsZTab, sizeof(float) * numberOfCentroids,cudaMemcpyDeviceToHost));
	return centroids;
}
int main(int argc, char** argv) {

	cout<<"startab";
	char c;
	while ((c = getopt(argc, argv, "c:p:")) != -1)
		switch (c) {
		case 'c':
			numberOfCentroids = atoi(optarg);
			break;
		case 'p':
			numberOfParticles = atoi(optarg);
			break;
		}

	Particles particles = RandParticles(numberOfParticles);

	Particles centroids = InitCentroids(particles, numberOfCentroids);

	InitKernel(particles, centroids);

	auto started = std::chrono::high_resolution_clock::now();

    ComputeCentroids(particles);

	auto done = std::chrono::high_resolution_clock::now();
	centroids=getCentroids();
	FreeDeviceMemory();
	DisplayParticles(centroids);
	cout << endl << "time:"
			<< std::chrono::duration_cast<std::chrono::milliseconds>(
					done - started).count() << endl;

	return 0;
}

